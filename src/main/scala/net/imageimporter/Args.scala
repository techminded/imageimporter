package net.imageimporter

import com.beust.jcommander.Parameter

object Args {
  @Parameter(
    names = Array("-f", "--files"),
    description = "Files to load. Can be specified one or multiple times.")
  var files: java.util.List[String] = null
  @Parameter(
    names = Array("-h", "--dbhost"),
    description = "Database host")
  var dbhost: String = "localhost"
  @Parameter(
    names = Array("-d ", "--dbname"),
    description = "Database name")
  var dbname: String = ""
  @Parameter(
    names = Array("-u", "--dbuser"),
    description = "Database host")
  var dbuser: String = "root"
  @Parameter(
    names = Array("-p", "--dbpassword"),
    description = "Database password")
  var dbpassword: String = ""
  @Parameter(
    names = Array("-a", "--actions"),
    description = "Actions to perform")
  var actions: java.util.List[String] = null
}