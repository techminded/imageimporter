package net.imageimporter

import java.awt.image.BufferedImage
import java.io.{ByteArrayOutputStream, FileInputStream, File}
import javax.imageio.ImageIO
import com.beust.jcommander.{JCommander, Parameter}
import collection.JavaConversions._

import java.sql.{Connection, DriverManager, ResultSet}


object Main  {

  def importFromFiles(fileNames: java.util.List[String], dbhost: String, dbname: String, dbuser: String, dbpassword: String) {
    val conn_str = "jdbc:mysql://" + dbhost + ":3306/"+ dbname +"?user=" + dbuser + "&password=" + dbpassword

    classOf[com.mysql.jdbc.Driver]

    val conn = DriverManager.getConnection(conn_str)
    try {

      val statement = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)

      for (fileName <- fileNames) {
        val fileExtension = fileName.replaceAll("^.*\\.([^.]+)$", "$1")

        val fios = new FileInputStream(new File(fileName))

        val prep = conn.prepareStatement(
          "INSERT INTO media_file (content_type, data, file_name) VALUES (?, ?, ?)"
        )

        prep.setString(1, "image/" + fileExtension)
        prep.setBlob(2, fios)
        prep.setString(3, fileName)
        prep.executeUpdate
      }

    } catch {
      case e: Exception => {
        println("\n" + e)
        println("\n" + e.getStackTrace + "\n")
      }
    } finally {
      conn.close
    }

  }
  def main(args: Array[String]) {
    new JCommander(Args, args.toArray: _*)
    Args.actions match {
      case List("import") => {
        importFromFiles(Args.files, Args.dbhost, Args.dbname, Args.dbuser, Args.dbpassword)
      }
      case _ => {
        importFromFiles(Args.files, Args.dbhost, Args.dbname, Args.dbuser, Args.dbpassword)
      }
    }
  }
}
