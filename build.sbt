import AssemblyKeys._

assemblySettings

mainClass in assembly := Some("net.imageimporter.Main")

//org.scalastyle.sbt.ScalastylePlugin.Settings

name := "imageimporter"

version := "0.1"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "com.beust" % "jcommander" % "1.35",
  "mysql" % "mysql-connector-java" % "5.1.12"
  )

scalacOptions ++= Seq("-unchecked", "-deprecation")
