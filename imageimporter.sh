#!/bin/bash



while getopts ":t:h:d:u:p:" opt; do
  case $opt in
    t) FILES="$OPTARG"
    ;;
    h) HOST="$OPTARG"
    ;;
    d) DATABASE="$OPTARG"
    ;;
    u) USR="$OPTARG"
    ;;
    p) PW="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

FILES=$FILES/*
for f in $FILES
do
  echo "executing command: java -jar imageimorter-assembly-0.1.jar -a import -h $HOST -u $USR -d $DATABASE -p $PW -f $f"
  java -jar imageimorter-assembly-0.1.jar -a import -h $HOST -u $USR -d $DATABASE -p $PW -f $f
done
